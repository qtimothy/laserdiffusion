# -*- coding: utf-8 -*-
"""
Created on Tue Nov 27 08:45:28 2018

@author: Tim
"""
from laser_diffusion_functions import image_take,transform_image,finding_dotty,\
                                      catch_line,pde_fun,line_finder
from scipy.optimize import curve_fit
from datetime import datetime
import numpy as np
import time 
import matplotlib.pyplot as plt
from scipy.stats.distributions import  t
from scipy.stats.distributions import  t
import cv2
import copy
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from skimage.filters import threshold_minimum
from skimage.measure import label, regionprops
from skimage.morphology import closing, square
from skimage.color import label2rgb
from skimage.segmentation import clear_border
from skimage import transform as tf
from scipy.optimize import curve_fit
plt.close('all')  #close all open plots, start fresh
#
#def time_diffusion(max_time,pts,path):
#    Export_Path = path
#    eo = open(Export_Path,'w')
#    pts = 10
#    max_seconds = max_time*3600
#    start_time = t.time()
#    dt = max_seconds/pts
#    current_time = t.time()-start_time
#    while max_seconds>=current_time:
#        I = image_take(cam_input=1)
#
#
#
#
#        current_time = t.time()-start_time
#        print(current_time)
#        data = np.random.normal(0,1)
#        text = str(current_time)+','+str(data)+'\n'
#        eo.write(text)
#        t.sleep(dt)
#    eo.close()

#I,s = image_take(0)



#ptsloc = finding_dotty(I,4)
#Tran_Im = transform_image(I,ptsloc)
#x,y = catch_line(Tran_Im)
#

f, ax = plt.subplots(3, 2) #create a 2 x 2 set of subplots (4 total)
ax[0,0].set_title('Raw Image') #set the figure title
ax[0,0].imshow(I)  #show the image
ax[0,0].axis('off') #images need no axis


pts = 4
Igg=I.copy() #going to make BW img out of green channel
Igg[:,:,0]=Igg[:,:,1]  #red and blue get same values as green channel
Igg[:,:,2]=Igg[:,:,1]
thresh=threshold_minimum(Igg)*0.5;  #cutoff between B & W
IBinary = (Igg[:,:,2] > thresh) #array where true at brightness > threshold


IBinaryFix = clear_border(1- closing(IBinary,square(3)))
label_image = label(IBinaryFix)# find and lable all white contiguous shapes
region_properties = regionprops(label_image)#get properties of labeled regions
#For all the properties you can assess see: 
#         http://scikit-image.org/docs/dev/api/skimage.measure.html
n = len(region_properties) #number of objects detected
d=np.zeros(n) #container for our diameters
e=np.zeros(n) #container for our eccentricities
i = 0
dim = np.shape(I)
tol =0.001*(dim[0]*dim[1])
param_save = np.zeros([n,4])
ax[0,1].set_title('Finding Dots') #set the figure title
ax[0,1].imshow(IBinary)  #show the image
ax[0,1].axis('off') #images need no axis

for region in region_properties:
    # take regions with large enough areas
    if region.filled_area >= tol:
        # draw rectangle around segmented coins
        minr, minc, maxr, maxc = region.bbox  #object's bounding box
        rect = mpatches.Rectangle((minc,minr),maxc-minc,maxr-minr,fill=False,edgecolor='white',linewidth=1)
        ax[0,1].add_patch(rect)
        param_save[i,:] = np.array([minr, minc, maxr, maxc])
        d[i]=(minr+maxr)/2  #calling diameter avg of min and max r
        e[i]= region.eccentricity  #closer to 0 is closer to circle
        i+=1  #advance counter
region_investigate = np.nonzero(e)[0]
save_loc = np.zeros([pts,2])
ecopy = copy.deepcopy(e[region_investigate])
for i in range(0,pts,1):
    eloc = np.where(ecopy==np.min(ecopy[region_investigate]))[0]
    ecopy[eloc] = 10
    minr, minc, maxr, maxc = param_save[eloc[0],:]             
    save_loc[i,:] = np.array([np.mean([minc,maxc]),np.mean([minr,maxr])])
store_logic = np.sum(save_loc,axis=1)
tranform_loc = np.copy(save_loc)
for i in range(0,pts,1):
    order_loc = np.where(np.min(store_logic)==store_logic)
    store_logic[order_loc]= np.max(store_logic)+1000
    if i==2:
        tranform_loc[3,:]= save_loc[order_loc,:]
    elif i==3:    
        tranform_loc[2,:]= save_loc[order_loc,:]
    else:
        tranform_loc[i,:]= save_loc[order_loc,:]
ax[0,1].plot(tranform_loc[:,0],tranform_loc[:,1],'or',markersize=1)

tranformimg = transform_image(I,tranform_loc)
ax[1,0].set_title('Transformed Image') #set the figure title
ax[1,0].imshow(tranformimg)  #show the image
ax[1,0].axis('off') #images need no axis





Igg=tranformimg.copy() #going to make BW img out of green channel
Igg[:,:,0]=Igg[:,:,1]  #red and blue get same values as green channel
Igg[:,:,2]=Igg[:,:,1]
thresh=0.99;  #cutoff between B & W
IBinary = (Igg[:,:,2] < thresh) #array where true at brightness > threshold    
ax[1,1].set_title('Line Capture') #set the figure title
ax[1,1].imshow(IBinary)  #show the image
ax[1,1].axis('off') #images need no axis


see=np.where(IBinary==False)



#
#
#path = r"D:\Documents\laser_data\y.npy"
#y = np.load(path)
#path = r"D:\Documents\laser_data\x.npy"
#x = np.load(path)


#plt.plot(x,y,'ok')


x = see[1]
y = see[0]

ax[2,0].set_title('Line Capture') #set the figure title
ax[2,0].axis('off') #images need no axis


#
#
##

x = x-np.min(x)
x = x/np.max(x)
y = y-np.min(y)
y = y/np.max(y)

x1 = np.min(x)
x2 = np.max(x)

low_loc = np.min(np.where(x1==x)[0])
high_loc = np.max(np.where(x2==x)[0])
y2 = y[high_loc]
y1 = y[low_loc]
#
dt = 35*60
xplot = np.linspace(0,1,1000)



m,b = line_finder(x1,x2,y1,y2)  
yplot = m*xplot+b

yadj = y-(m*x+b)  
ax[2,0].plot(xplot,yplot,'--b',label='Correction Line')
ax[2,0].plot(x,yadj,'^r',markersize=2,label = 'Adjusted Curve')
ax[2,0].plot(x,y,'ok',markersize=2,alpha=0.5,label = 'Raw Curve')
ax[2,0].legend()

popt,pcov = curve_fit(pde_fun,x,yadj,p0 = [0.2  ,-1,-1])
C1,C2,C3 = popt
y_model = pde_fun(xplot,C1,C2,C3)
ax[2,1].set_title('Curve Fit') #set the figure title

ax[2,1].plot(x,yadj,'^r',markersize=2,label = 'Data')
ax[2,1].plot(xplot,y_model,'k',label = 'Model')
ax[2,1].axis('off')
ax[2,1].legend()
n = len(x)    # number of data points
p = len(pcov) # number of parameters
dof = max(0, n - p) # number of degrees of freedom
alpha = 0.05
# student-t value for the dof and confidence level
tval = t.ppf(1.0 - 0.5 * alpha, dof)
sigma = np.diag(pcov) ** 0.5
moe = tval * sigma
#
D_2 = -1/(dt*4*C2)
D_2u =1/(dt*4*moe[1]) 
text = ('$D_{AB}$=%0.1e'%D_2)+('$\pm%0.1e$'%D_2u)
equation = '$y = C_1\exp(C_2(x+C_3)^2)$'
f, ax = plt.subplots(2, 1) #create a 2 x 2 set of subplots (4 total)
ax[0].plot(x,yadj,'^r',markersize=2,label = 'Data')
ax[0].plot(xplot,y_model,'k',label = 'Model')
ax[0].text(0.5,0.4,equation) 
ax[0].text(0.5,0.3,text)
con = ['$C_1$','$C_2$','$C_3$']
for i in range(0,len(moe)):
    text = con[i]+(' = %0.2f'%popt[i])+('$\pm$ %0.2e'%moe[i])
    ax[0].text(0.5,0.3-(i+1)*0.1,text)
ax[0].axis('off')
ax[0].legend()



ax[1].plot(x,yadj,'^r',markersize=2,label = 'Data')

n = 1000
C1_rand = np.random.normal(C1,moe[0],n)
C2_rand = np.random.normal(C2,moe[1],n)
C3_rand = np.random.normal(C3,moe[2],n)

for i in range(0,n,1):
    y_modelrand = pde_fun(xplot,C1_rand[i],C2_rand[i],C3_rand[i])
    ax[1].plot(xplot,y_modelrand,'--b',alpha = 0.01)

ax[1].plot(xplot,y_modelrand,'--b',alpha = 0.5,label = 'Uncertainty')
ax[1].plot(xplot,y_model,'k',label = 'Model')
ax[1].axis('off')
ax[1].legend()

#
