# -*- coding: utf-8 -*-
"""
Created on Thu Mar 29 16:19:13 2018

@author: tony3
"""
#first required:  pip install opencv-python
import matplotlib.pyplot as plt
from laser_diffusion_functions import image_take




plt.close('all')  #close all open plots, start fresh
for i in range(0,10,1):
    s,im = image_take()
    #cv2.imshow("Test Picture", im) # displays captured image
    #cv2.imwrite("test.bmp",im) # writes image test.bmp to disk
    f, ax = plt.subplots(2, 2) #create a 2 x 2 set of subplots (4 total)
    ax[0,0].set_title('Raw Img') #set the figure title
    ax[0,0].imshow(im)  #show the image

