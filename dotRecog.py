# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 16:35:51 2018

@author: rtlaw
"""

import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from skimage.filters import threshold_minimum
from skimage.measure import label, regionprops
from skimage.morphology import closing, square
from skimage.color import label2rgb
from skimage.segmentation import clear_border

plt.close('all')  #close all open plots, start fresh

####    YOU MUST CHANGE THE PATH STRING BELOW TO LOAD YOUR BEAD PHOTO     ####
#########################################################################
I = sp.misc.imread("20181113_155052.jpg")
Igg=I.copy() #going to make BW img out of green channel
Igg[:,:,0]=Igg[:,:,1]  #red and blue get same values as green channel
Igg[:,:,2]=Igg[:,:,1]
thresh=threshold_minimum(Igg[:,:,2])*1;  #cutoff between B & W
IBinary = (Igg[:,:,2] > thresh) #array where true at brightness > threshold

IBinaryFix = clear_border(1- closing(IBinary,square(3)))
label_image = label(IBinaryFix)# find and lable all white contiguous shapes
image_label_overlay = label2rgb(label_image, I) #combine with original img

f, ax = plt.subplots(1, 1) 
ax.imshow(I)  #plot the overlay image

region_properties = regionprops(label_image)#get properties of labeled regions
#For all the properties you can assess see: 
#         http://scikit-image.org/docs/dev/api/skimage.measure.html
n = len(region_properties) #number of objects detected
d=np.zeros(n) #container for our diameters
e=np.zeros(n) #container for our eccentricities
i=0  # a counter
for region in region_properties:
    # take regions with large enough areas
    if region.area >= 10000:
        # draw rectangle around segmented coins
        minr, minc, maxr, maxc = region.bbox  #object's bounding box
        rect = mpatches.Rectangle((minc, minr), maxc - minc, maxr - minr,
                                  fill=False, edgecolor='white', linewidth=1)
        ax.add_patch(rect)   #outline object with rectangle
        d[i]=(minr+maxr)/2  #calling diameter avg of min and max r
        e[i]= region.eccentricity  #closer to 0 is closer to circle
        i+=1  #advance counter
