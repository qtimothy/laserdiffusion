# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 14:16:10 2018

@author: tony3
"""
from scipy.stats.distributions import  t
import cv2
import copy
import numpy as np
from skimage.filters import threshold_minimum
from skimage.measure import label, regionprops
from skimage.morphology import closing, square
from skimage.segmentation import clear_border
from skimage import transform as tf
from scipy.optimize import curve_fit


"""
Function used to take an image uses the default of 1 refering to the port
If this does not work please change the cam_input this return s and the image
"""
def image_take(cam_input=1):
    cam = cv2.VideoCapture(cam_input)
    s, im = cam.read() # captures image
    cam.release() 
    return im,s

"""
Function finds the dots by specifiying the number of dots in the given image
"""


def finding_dotty(I,pts):
    Igg=I.copy() #going to make BW img out of green channel
    Igg[:,:,0]=Igg[:,:,1]  #red and blue get same values as green channel
    Igg[:,:,2]=Igg[:,:,1]
    thresh=threshold_minimum(Igg)*0.5;  #cutoff between B & W ####MAY NEED TO CHANGE IF IT DOESNT WORK
    IBinary = (Igg[:,:,2] > thresh) #array where true at brightness > threshold
    
    
    IBinaryFix = clear_border(1- closing(IBinary,square(3)))
    label_image = label(IBinaryFix)# find and lable all white contiguous shapes
    region_properties = regionprops(label_image)#get properties of labeled regions
    #For all the properties you can assess see: 
    #         http://scikit-image.org/docs/dev/api/skimage.measure.html
    n = len(region_properties) #number of objects detected
    d=np.zeros(n) #container for our diameters
    e=np.zeros(n) #container for our eccentricities
    i = 0
    dim = np.shape(I)
    tol =0.001*(dim[0]*dim[1]) #####May need to change if it doesnt work based on picture and dot size 
    param_save = np.zeros([n,4])
    for region in region_properties:
        # take regions with large enough areas
        if region.filled_area >= tol:
            # draw rectangle around segmented coins
            minr, minc, maxr, maxc = region.bbox  #object's bounding box
            
            param_save[i,:] = np.array([minr, minc, maxr, maxc])
            d[i]=(minr+maxr)/2  #calling diameter avg of min and max r
            e[i]= region.eccentricity  #closer to 0 is closer to circle
            i+=1  #advance counter
    region_investigate = np.nonzero(e)[0]
    save_loc = np.zeros([pts,2])
    ecopy = copy.deepcopy(e[region_investigate])
    
    ###THis portion of the code finds the objects that have the lowest eccentricities
    
    for i in range(0,pts,1):
        eloc = np.where(ecopy==np.min(ecopy[region_investigate]))[0]
        ecopy[eloc] = 10
        minr, minc, maxr, maxc = param_save[eloc[0],:]             
        save_loc[i,:] = np.array([np.mean([minc,maxc]),np.mean([minr,maxr])])
    store_logic = np.sum(save_loc,axis=1)
    tranform_loc = np.copy(save_loc)
        ###THis portion of the code orders the points for transform image 

    for i in range(0,pts,1):
        order_loc = np.where(np.min(store_logic)==store_logic)
        store_logic[order_loc]= np.max(store_logic)+1000
        if i==2:
            tranform_loc[3,:]= save_loc[order_loc,:]
        elif i==3:    
            tranform_loc[2,:]= save_loc[order_loc,:]
        else:
            tranform_loc[i,:]= save_loc[order_loc,:]
    return tranform_loc

"""
Function used to transform the image using the dots takes four points and returns an warped or transposed image
"""

def transform_image(I,dst):
    y,x,z = np.shape(I) #image in to xyz
    src = np.array([[0, 0], [0, y-1], [x-1, y-1], [x-1, 0]]) #gives an intial guess
    tform3 = tf.ProjectiveTransform() #transfroms
    tform3.estimate(src, dst) #does the actual transformation
    warped = tf.warp(I, tform3, output_shape=(y, x)) #returns warped image 
    return warped

"""
Function used to extract data takes transformed image 
"""



def catch_line(I):
    Igg=I.copy() #going to make BW img out of green channel
    Igg[:,:,0]=Igg[:,:,1]  #red and blue get same values as green channel
    Igg[:,:,2]=Igg[:,:,1]
    thresh=0.999;  #cutoff between B & W MAY NEED TO CHANGE may need to be lowered
    IBinary = (Igg[:,:,2] < thresh) #array where true at brightness > threshold    

    
    see=np.where(IBinary==False)
    return see[1],see[0]


"""
Functions used to fit the line 
"""


def line_finder(x1,x2,y1,y2):
    m = (y2-y1)/(x2-x1)
    b = y2-m*x2
    
    return m,b

def pde_fun(x,C1,C2,C3):
    return C1*np.exp(C2*(x+C3)**2)

"""
Functions used to find diffusifity 
"""

def diffuse_fit(x,y,ti,t0):
   
    
    #conduct normalizaton
    x = x-np.min(x)
    x = x/np.max(x)
    y = y-np.min(y)
    y = y/np.max(y)
    
    x1 = np.min(x)
    x2 = np.max(x)
    
    low_loc = np.min(np.where(x1==x)[0])
    high_loc = np.max(np.where(x2==x)[0])
    y2 = y[high_loc]
    y1 = y[low_loc]
    #
    dt = ti-t0
    
    
    #adjust line
    m,b = line_finder(x1,x2,y1,y2)  
    
    yadj = y-(m*x+b)  
    # fit to line
    popt,pcov = curve_fit(pde_fun,x,yadj,p0 = [0.2  ,-1,-1])
    C1,C2,C3 = popt
    n = len(x)    # number of data points
    p = len(pcov) # number of parameters
    dof = max(0, n - p) # number of degrees of freedom
    alpha = 0.05
    # student-t value for the dof and confidence level
    tval = t.ppf(1.0 - 0.5 * alpha, dof)
    sigma = np.diag(pcov) ** 0.5
    moe = tval * sigma
    #uncertianity analysis
    D_2 = -1/(dt*4*C2)
    D_2p =1/(dt*4*(moe[1]+D_2)) 
    D_2n =1/(dt*4*(moe[1]-D_2))
    D_2u = 0.5*(abs(D_2-D_2p)+abs(D_2-D_2n))
    return D_2,D_2u