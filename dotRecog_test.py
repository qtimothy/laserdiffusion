# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 16:35:51 2018

@author: rtlaw
"""
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
from laser_diffusion_functions import transform_image,finding_dotty,catch_line
plt.close('all')  #close all open plots, start fresh

####    YOU MUST CHANGE THE PATH STRING BELOW TO LOAD YOUR BEAD PHOTO     ####
#########################################################################

pts = 4
tranform_loc = finding_dotty(I,pts)
It = transform_image(I,tranform_loc)
x,y = catch_line(It)

minloc = np.min()

plt.figure()
plt.plot(x,y,'r')


