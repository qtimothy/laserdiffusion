# -*- coding: utf-8 -*-
"""
Created on Fri Jan 26 08:41:53 2018

@author: tony3
"""
# STUFF I WANNA USE 
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from laser_diffusion_functions import image_take
from skimage.filters import threshold_minimum
from skimage import data, color
from skimage.transform import hough_circle, hough_circle_peaks
from skimage.feature import canny
from skimage.draw import circle_perimeter
from skimage.util import img_as_ubyte
from skimage.morphology import closing,square
from skimage.segmentation import clear_border

from skimage import transform as tf



plt.close('all')  #close all open plots, start fresh

####    YOU MUST CHANGE THE PATH STRING BELOW TO LOAD YOUR ALGAE PHOTO     ####
#########################################################################
I,s = image_take(0)
##########################################################################

Ir = I.copy() #red channel
Ir[:,:,1]=0  #Image files are 3 D matrix where  0 = red, 1 = green, 2 = blue
Ir[:,:,2]=0
Ig = I.copy() #green channel, really all we need but want to show you diff
Ig[:,:,0]=0 # set reds to zero
Ig[:,:,2]=0 # set blues to zero
Ib = I.copy()  #blue channel
Ib[:,:,0]=0
Ib[:,:,1]=0

f, ax = plt.subplots(2, 2) #create a 2 x 2 set of subplots (4 total)
ax[0,0].set_title('Raw Img') #set the figure title
ax[0,0].imshow(I)  #show the image
ax[0,0].axis('off') #images need no axis
ax[0,1].set_title('Red Channel') #set the title for red
ax[0,1].imshow(Ir)
ax[0,1].axis('off')
ax[1,0].set_title('Green Channel') #set the title for green
ax[1,0].imshow(Ig)
ax[1,0].axis('off')
ax[1,1].set_title('Blue Channel') #set the title for blue
ax[1,1].imshow(Ib)
ax[1,1].axis('off')


dst = np.array([[80, 60], [60, 420], [550, 420], [530, 90]])



def transform_image(I,dst):
    y,x,z = np.shape(I)
    src = np.array([[0, 0], [0, y-1], [x-1, y-1], [x-1, 0]])
    tform3 = tf.ProjectiveTransform()
    tform3.estimate(src, dst)
    warped = tf.warp(I, tform3, output_shape=(y, x))
    return warped


warped = transform_image(I,dst)
fig, ax = plt.subplots(nrows=3, figsize=(8, 3))

ax[0].imshow(I, cmap=plt.cm.gray)
ax[0].plot(dst[:, 0], dst[:, 1], '.r')

ax[1].imshow(warped, cmap=plt.cm.gray)
for a in ax:
    a.axis('off')

plt.tight_layout()

plt.show()

