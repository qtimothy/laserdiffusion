# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 13:32:59 2018

@author: rtlaw
"""
# STUFF I WANNA USE 
import scipy as sp
import numpy as np
import matplotlib.pyplot as plt

plt.close('all')  #close all open plots, start fresh

####    YOU MUST CHANGE THE PATH STRING BELOW TO LOAD YOUR BEAD PHOTO     ####
#########################################################################
I = sp.misc.imread("20181113_155052.jpg")
##########################################################################

def convert_image_to_data(value, threshold, image_name):
    # value is based on green channel color.
    # To update for different color, just switch "value" to the a different channel
    # and replace with query.
    img = sp.misc.imread(image_name)
    r = 0 #red channel
    g = 1 #green channel
    b = 2 #blue channel
    if value < 0:
        value = 0
    if value > 255:
        value = 255
    query = value - threshold
    if query < 0:
        query = 0
    array = np.where((img[:,:,r] <= query) & (img[:,:,g] >= value) & (img[:,:,b] <= query))
    return array

# example
test = convert_image_to_data(100, 60, "20181113_155052.jpg")
plt.scatter(test[0],test[1])